### 0.1.0 (2021-09-14)

##### Build System / Dependencies

*  update repo url ([f459e083](https://gitlab.com/luisbar/wiigo-dashboard/commit/f459e08303e870f208e91cb1ee15f0a9f77df543))
*  add/remove some packages and add new eslint scripts ([202949a0](https://gitlab.com/luisbar/wiigo-dashboard/commit/202949a0345d745cfcec392b91850ae31c55549e))

##### Documentation Changes

*  remove changelog ([a62354a1](https://gitlab.com/luisbar/wiigo-dashboard/commit/a62354a1824948e50076f58886ca190702440cdc))

##### New Features

*  use env var for disabling/enabling devtools ([7572a670](https://gitlab.com/luisbar/wiigo-dashboard/commit/7572a6708af073b4c7a37325406885cbac01d326))
*  add cars view ([36796901](https://gitlab.com/luisbar/wiigo-dashboard/commit/36796901fc8725e1170768a70e49f19e9c05d751))
*  update trip component and create machine for it ([906a36eb](https://gitlab.com/luisbar/wiigo-dashboard/commit/906a36eb41ef7d0450257cea2086db0df33a34d0))
*  create quotationform ([d6db7e0d](https://gitlab.com/luisbar/wiigo-dashboard/commit/d6db7e0da06c7e274edc2e2daa77dc5903c401b8))
*  add schema for validationg the quotation form data ([f502be6a](https://gitlab.com/luisbar/wiigo-dashboard/commit/f502be6abd1faf73ba7fcfd3a2504305c6a292f0))
*  add patch method to requester class ([d01369e6](https://gitlab.com/luisbar/wiigo-dashboard/commit/d01369e6ff70e69fe57381103205fc18b856d7f2))
*  add validator schema to filter form component ([d5aff791](https://gitlab.com/luisbar/wiigo-dashboard/commit/d5aff7915d92e7f9b1c5ba0f2603252944bf780f))
*  add error to dropdown component ([75c0e0cb](https://gitlab.com/luisbar/wiigo-dashboard/commit/75c0e0cb00fecbb11edf340e042a49516f480c77))
*  add card and map to trip component ([812551eb](https://gitlab.com/luisbar/wiigo-dashboard/commit/812551eb980bee74ae9324d822008bce48ac8310))
*  create card component ([f74d64e0](https://gitlab.com/luisbar/wiigo-dashboard/commit/f74d64e0ad9d2520f8774bdd4d8dfef38098f1fc))
*  add vertical gap to grid of main template ([636b1682](https://gitlab.com/luisbar/wiigo-dashboard/commit/636b16821d36d4cc7ae79df45950a8f8045810da))
*  add more colors to tailwind config file ([8a3c4513](https://gitlab.com/luisbar/wiigo-dashboard/commit/8a3c45138a34b049e46f84b28937d4ae5f3fb86e))
*  create map component using leaflet ([c36c5a18](https://gitlab.com/luisbar/wiigo-dashboard/commit/c36c5a185945bf195024d392c7d24561297d54dc))
*  add table cell to table component and improve the table component ([29245872](https://gitlab.com/luisbar/wiigo-dashboard/commit/2924587204c4c4223e8ed2db51d9adc838929b9d))
*  create table cell component ([0b6e70e9](https://gitlab.com/luisbar/wiigo-dashboard/commit/0b6e70e9836c78c43575ec4abdbe3c5fd509fd60))
*  create and add internalization provider in order to pass language by parameter ([8478d940](https://gitlab.com/luisbar/wiigo-dashboard/commit/8478d9402e5956fec6b2cb8e5cb30760368aed79))
*  add validator to loginform component ([81baa542](https://gitlab.com/luisbar/wiigo-dashboard/commit/81baa5422975321dfb87c881fdce26256b4329e3))
*  add schema validator ([13f9b03b](https://gitlab.com/luisbar/wiigo-dashboard/commit/13f9b03b5cc4dd0047a7222aa88eaed2759ad8c2))
*  change trips component in order to use the store provider ([f1c603fe](https://gitlab.com/luisbar/wiigo-dashboard/commit/f1c603fe8fd190619c770082c51179bbcd280146))
*  change login in order to use the store provider ([4fc2ac6e](https://gitlab.com/luisbar/wiigo-dashboard/commit/4fc2ac6e8caa9adcbca3fe79dd79dbd5c0e35a2b))
*  create storeprovider using context ([1a5ad57d](https://gitlab.com/luisbar/wiigo-dashboard/commit/1a5ad57db8fc3f199d4ea1d66aa4bf21d85df5dd))
*  add dotenv plugin to webpack and use it in order to inject env vars at build time ([d4b56dac](https://gitlab.com/luisbar/wiigo-dashboard/commit/d4b56daccd3bbf697a4edfe67d5d58d442bc9d50))
*  add paginator and filterform components to home component ([45fa73f3](https://gitlab.com/luisbar/wiigo-dashboard/commit/45fa73f34600bd336f01e324c9550b13b32567a8))
*  create filterform component and its state machine ([e68c1718](https://gitlab.com/luisbar/wiigo-dashboard/commit/e68c17189b3310e1ef00e5a55af2244c36b711b7))
*  add search property to paginator component ([ab2a5851](https://gitlab.com/luisbar/wiigo-dashboard/commit/ab2a5851a5ede116709460aec3f85e4a4b296c2b))
*  create paginator component ([a0ea993d](https://gitlab.com/luisbar/wiigo-dashboard/commit/a0ea993d3dbeb146b70422ab3550fa4b74c97c5d))
*  add classname property to table component ([037ad663](https://gitlab.com/luisbar/wiigo-dashboard/commit/037ad66342fcc3a02ea21f1a5ef756d727dd75e9))
*  add usedisabled hook to input component ([8c24ff0b](https://gitlab.com/luisbar/wiigo-dashboard/commit/8c24ff0bdf2047b37521d0b9015bcc20349f1009))
*  add two states to button and add usedisabled hook to it ([696ce334](https://gitlab.com/luisbar/wiigo-dashboard/commit/696ce3349b4af7ad0ac03b6174e9787cbdcaec50))
*  create dropdown component ([75357b9b](https://gitlab.com/luisbar/wiigo-dashboard/commit/75357b9bf65f3e1ed0c3ee8495fc57c386e0f786))
*  add hook for changing a state machine to disabled or enabled state ([27c27340](https://gitlab.com/luisbar/wiigo-dashboard/commit/27c27340c46ebb23d6e9c247ba7292bd30f5eb7a))
*  add hook folder and alias ([0d7e1c43](https://gitlab.com/luisbar/wiigo-dashboard/commit/0d7e1c431cbf81bc38e2d59cd35633d2b37aa0b6))
*  enable alias resolver and intellisense for react ([12f1b02a](https://gitlab.com/luisbar/wiigo-dashboard/commit/12f1b02aae5cd76f5c3ef6f2bf8abec1dee44b60))
*  use grid layout instead of a table in the table component ([ce34443c](https://gitlab.com/luisbar/wiigo-dashboard/commit/ce34443cad0ebbcc0f0861807d36ae9617318e5d))
*  add pagination to get trips method ([64f20c58](https://gitlab.com/luisbar/wiigo-dashboard/commit/64f20c58828669f067991f6e691d97fc28dc7edf))
*  fetch trips from the api ([d7ee584f](https://gitlab.com/luisbar/wiigo-dashboard/commit/d7ee584f54bce6fec0f5f15ed688daa375c7cffe))
*  move login logic to loginform and integrate with the api ([6882d272](https://gitlab.com/luisbar/wiigo-dashboard/commit/6882d27288334bf7c3bd4d604725e8b30e5e1ac0))
*  add log out functionality ([eae76b68](https://gitlab.com/luisbar/wiigo-dashboard/commit/eae76b68966c139f965aa5b14b78276655e07ef7))
*  add login view ([8e5e909b](https://gitlab.com/luisbar/wiigo-dashboard/commit/8e5e909b4c3525f1247453e65a7a6b94e469151c))

##### Bug Fixes

*  fix lintstaged configuration ([deb0312e](https://gitlab.com/luisbar/wiigo-dashboard/commit/deb0312e5fef506e3c20c025be746868ee527f88))
*  move alert and spinner to trip from quotationform component ([e78113f9](https://gitlab.com/luisbar/wiigo-dashboard/commit/e78113f91b2cbad1131b7c27fe68cdd2a5b85008))
*  fix guard condition of paginator machine ([62ea2239](https://gitlab.com/luisbar/wiigo-dashboard/commit/62ea2239d6827c12dba08ff674d163d826a73369))
*  add fallback to card component when string is falsy ([c2724010](https://gitlab.com/luisbar/wiigo-dashboard/commit/c2724010ca5877b224ec14466922d5c2a79589c4))
*  move error to top side of input and dropdown component ([e2ec2bd4](https://gitlab.com/luisbar/wiigo-dashboard/commit/e2ec2bd410e0cce111a6ba73a609bd26982ae3ba))
*  add missing parameter to process event of trips component ([456fc4ad](https://gitlab.com/luisbar/wiigo-dashboard/commit/456fc4ad490e2319dab31cd869a19fb972e01d39))
*  add minimun zoom to map ([40ed09ac](https://gitlab.com/luisbar/wiigo-dashboard/commit/40ed09aced60aa3ee06eb586fddbb5f9d8cac176))
*  add default value to filterform inputs ([ab1f1052](https://gitlab.com/luisbar/wiigo-dashboard/commit/ab1f1052531b28073d51332c522641888e88cc5e))
*  remove missing translation error of react intl adding a custom onerror listener ([ced4e8c8](https://gitlab.com/luisbar/wiigo-dashboard/commit/ced4e8c89694f4d1944d239fe3aeddafb05b7b85))
*  add missing event to final state of login and loginform machine ([45280dab](https://gitlab.com/luisbar/wiigo-dashboard/commit/45280dab1faf6b1fa1c43cf749246546a883ee5b))
*  get user data from cookies ([496c2a5f](https://gitlab.com/luisbar/wiigo-dashboard/commit/496c2a5fc29afae33d94da8b65ba9d01a7222890))
*  add onchange property and fix value ([f980e22e](https://gitlab.com/luisbar/wiigo-dashboard/commit/f980e22ef084c27f9e6b6f03e46bf771afce5219))
*  update loginform component and its state machine ([46e90655](https://gitlab.com/luisbar/wiigo-dashboard/commit/46e90655cf43c682164e500e43590d13a25ec30c))
*  change payload by data due to compatibility ([5c06359a](https://gitlab.com/luisbar/wiigo-dashboard/commit/5c06359abe65e9a555236980dab44e8869e3caac))
*  add text component to globalalert component ([a4aa7a34](https://gitlab.com/luisbar/wiigo-dashboard/commit/a4aa7a3488781aba4e9efe068f1a735911d46499))

##### Other Changes

*  update eslint configuration ([3e504fe8](https://gitlab.com/luisbar/wiigo-dashboard/commit/3e504fe8a13996b4f8c8e527918929fe572fad48))
*  add new env var to .env file ([ff3b5285](https://gitlab.com/luisbar/wiigo-dashboard/commit/ff3b5285a0a2c9b2ce1503f6f14dd16a56ed0eeb))
*  install leaflet and ramda ([9dd008ef](https://gitlab.com/luisbar/wiigo-dashboard/commit/9dd008efd3a20e6c7f28136392f5db4b86e547c1))
*  create dummy trip page ([3a39ec1b](https://gitlab.com/luisbar/wiigo-dashboard/commit/3a39ec1b04301e23ba5527bc63ad76ab296401a2))
*  install react icons ([b6ee1702](https://gitlab.com/luisbar/wiigo-dashboard/commit/b6ee17028a8f82fa358090ac5c6543940624bff5))

##### Refactors

*  update url for trips and trip views ([e3d14d94](https://gitlab.com/luisbar/wiigo-dashboard/commit/e3d14d94f236c755f3d1f1c1066509961402595d))
*  remove retries from trips component ([be21a060](https://gitlab.com/luisbar/wiigo-dashboard/commit/be21a0601e51384c92a417c5a39bdf5788285a13))
*  remove unused method from index ([64572003](https://gitlab.com/luisbar/wiigo-dashboard/commit/64572003162c334c905912e8aca7eff3cb45fc75))
*  change statuses of login form component ([91f21d26](https://gitlab.com/luisbar/wiigo-dashboard/commit/91f21d26ef25eac0c9df44e34a7ff0967d572233))
*  move api call to login machine from login form machine ([eb49c12b](https://gitlab.com/luisbar/wiigo-dashboard/commit/eb49c12bffd444d3f7a3e579e32b59449a733ed9))
*  chage states of globalalert component ([664e5246](https://gitlab.com/luisbar/wiigo-dashboard/commit/664e524636d47c3e064f7140f10cfd4712e12ed2))
*  change states of globalspinner component ([5dd98736](https://gitlab.com/luisbar/wiigo-dashboard/commit/5dd987366bbd38c159d9e3a09d9d24b44fe0bc6c))
*  refactor all state machines ([fa72fd3a](https://gitlab.com/luisbar/wiigo-dashboard/commit/fa72fd3a6dc7029d2cb744d6defaed91a43b7f09))
*  change children id of logoutform ([a89a295b](https://gitlab.com/luisbar/wiigo-dashboard/commit/a89a295b327219f90b1eb0d0441c1ea7ab8cc8a1))
*  move menuitem to atoms folder ([e4c0af22](https://gitlab.com/luisbar/wiigo-dashboard/commit/e4c0af223e665f5e5a8e6c673f9272f096dab215))
*  move input to atoms folder ([284990ff](https://gitlab.com/luisbar/wiigo-dashboard/commit/284990fffdf7a24f402bfc2bd2f86351b05681bb))
*  move button to atoms folder ([3b6fef14](https://gitlab.com/luisbar/wiigo-dashboard/commit/3b6fef14db98f0dfa480c45e927ee8e2a996af23))
*  remove config folder from statemachines ([e8c135b3](https://gitlab.com/luisbar/wiigo-dashboard/commit/e8c135b3650df1053115f1dd78007796647c2628))

##### Code Style Changes

*  fix eslint issues ([187456c2](https://gitlab.com/luisbar/wiigo-dashboard/commit/187456c254fcb42c46123c9b557a571ed9a13710))
*  add zindex to map and global alert components ([5347d218](https://gitlab.com/luisbar/wiigo-dashboard/commit/5347d218b7691520bef822d4907fcb8c7781d6d1))
*  add grid to main template ([49cd95f0](https://gitlab.com/luisbar/wiigo-dashboard/commit/49cd95f0e6828a90c6b5bf45484ae48a2c0a1c3a))
*  add custom colors to button component ([ec24e6ae](https://gitlab.com/luisbar/wiigo-dashboard/commit/ec24e6aea056f46b6ab628d1d92e13b3252dda35))
*  define colors on tailwind config file ([4d31bf44](https://gitlab.com/luisbar/wiigo-dashboard/commit/4d31bf44bee08ebcea12d78a70c16df766011a57))
*  remove padding from table component ([f6bac258](https://gitlab.com/luisbar/wiigo-dashboard/commit/f6bac258dd5e657431e4f00333af8e5c59ef1cca))
*  improve styles of main template ([da43a93e](https://gitlab.com/luisbar/wiigo-dashboard/commit/da43a93ec0e03e8c430914f4f3dee63e2de0eead))
*  add fix width to sidemenu ([ee0751e3](https://gitlab.com/luisbar/wiigo-dashboard/commit/ee0751e386ffd26a2dd62e912ef77e321eb72d68))

