const colors = require('tailwindcss/colors')
const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: ['./src/**/*.{js,jsx,mdx}', './public/index.html'],
  darkMode: 'class',
  theme: {
    colors: {
      ...colors,
      'primary': '#818CF8',
      'primary-light': '#A5B4FB',
      'primary-dark': '#6466f1',
      'accent-100': '#F7B1AB',
      'accent-200': '#81E4DA',
      'accent-300': '#47E5BC',
      'light-100': '#FFFFFF',
      'dark-100': '#343434',
      'dark-200': '#3F3F46',
      'dark-300': '#CACACA',
      'dark-400': '#FAFAFA',
      'error': '#F8A5A5',
      'success': '#86EFAC',
    },
    extend: {
      spacing: defaultTheme.spacing,
      borderRadius: defaultTheme.borderRadius,
      gridTemplateRows: {
        'side-menu': '25% repeat(10, 5%)',
        'table': '1fr 6fr',
        'table-body-10': 'repeat(10, 5rem)',
        'table-body-20': 'repeat(20, 5rem)',
        'table-body-50': 'repeat(50, 5rem)',
        'main': '20% 70%',
      },
    },
  },
  variants: {
    extend: {
      backgroundColor: ['active', 'disabled'],
      cursor: ['disabled'],
      tableLayout: ['hover'],
      boxShadow: ['active'],
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
  corePlugins: {
    outline: false,
    ringWidth: false,
  },
}
