import axios from 'axios';
import getItemFromCookies from '@utils/getItemFromCookies';
import setCookie from '@utils/setCookie';

const UNAUTHORIZED = 401;

class Requester {
  constructor() {
    this.requester = axios.create({
      baseURL: process.env.BASE_URL,
    });

    this.requester.interceptors.response.use((config) => {
      if (config.status === UNAUTHORIZED) setCookie('token', '');
      return config;
    });
  }

  get(
    url,
  ) {
    return this.requester(
      {
        headers: {
          Authorization: `Bearer ${getItemFromCookies('token')}`,
        },
        url,
        method: 'GET',
      },
    );
  }

  post(
    url, data,
  ) {
    return this.requester(
      {
        headers: {
          Authorization: `Bearer ${getItemFromCookies('token')}`,
        },
        url,
        data,
        method: 'POST',
      },
    );
  }

  patch(
    url, data,
  ) {
    return this.requester(
      {
        headers: {
          Authorization: `Bearer ${getItemFromCookies('token')}`,
        },
        url,
        data,
        method: 'PATCH',
      },
    );
  }
}

export default new Requester();
