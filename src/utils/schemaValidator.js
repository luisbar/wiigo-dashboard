import joi from 'joi';

const getErrorMessages = ({ schema, data }) => {
  const { error } = schema.validate(data, { abortEarly: false });

  if (!error) return undefined;

  return error
    .details
    .map((detail) => `error.${detail.path[0]}.${detail.type}`);
};

const validateSessionData = (data = {}) => {
  const schema = joi.object({
    username: joi.string()
      .required()
      .email({ minDomainSegments: 2, tlds: { allow: [ 'com', 'net', 'io' ] } }),
    password: joi.string()
      .required()
      .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),
  });

  return getErrorMessages({ schema, data });
};

const validateFilterData = (data = {}) => {
  const schema = joi.object({
    pageSize: joi.number()
      .required()
      .max(50),
    filter: joi.string()
      .allow('')
      .optional()
      .alphanum()
      .max(50),
    pageNumber: joi.any()
      .optional(),
  });

  return getErrorMessages({ schema, data });
};

const validateQuotationData = (data = {}) => {
  const schema = joi.object({
    price: joi.number()
      .required(),
    status: joi.string()
      .optional(),
    tripId: joi.string()
      .optional(),
  });

  return getErrorMessages({ schema, data });
};

export default {
  validateSessionData,
  validateFilterData,
  validateQuotationData,
};