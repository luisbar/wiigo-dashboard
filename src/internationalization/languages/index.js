import en from '@internationalization/languages/en.json';
import es from '@internationalization/languages/es.json';

export {
  en,
  es,
};