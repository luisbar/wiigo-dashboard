import { createMachine, sendParent } from 'xstate';

export const machineDefinition = {
  id: 'button',
  initial: 'enabled',
  states: {
    enabled: {
      on: {
        PRESS: [
          {
            actions: [ 'sendParent' ],
            target: 'enabled',
          },
        ],
        DISABLE: [
          {
            target: 'disabled',
          },
        ],
      },
    },
    disabled: {
      on: {
        ENABLE: [
          {
            target: 'enabled',
          },
        ],
      },
    },
  },
};

const machineOptions = {
  actions: {
    sendParent: sendParent((context, event) => event.data),
  },
};

export default createMachine(machineDefinition, machineOptions);