import { actions, createMachine, send } from 'xstate';
import logOutFormMachine from '@stateMachines/organisms/logOutForm';
import filterFormMachine from '@stateMachines/organisms/filterForm';
import paginatorMachine from '@stateMachines/molecules/paginator';
import { assign } from '@xstate/immer';
import requester from '@utils/requester';

const CHILDREN_TO_DISABLED = [ 'filterForm', 'paginator' ];
const DEFAULT_PAGE_NUMBER = 0;
const DEFAULT_PAGE_SIZE = 10;

export const machineDefinition = {
  id: 'cars',
  initial: 'idle',
  context: {
    cars: [],
    error: {},
    pagination: {
      pageNumber: DEFAULT_PAGE_NUMBER,
      pageSize: DEFAULT_PAGE_SIZE,
      total: undefined,
    },
    filter: {},
  },
  invoke: [
    {
      id: 'logOutForm',
      src: logOutFormMachine,
    },
    {
      id: 'filterForm',
      src: filterFormMachine,
    },
    {
      id: 'paginator',
      src: paginatorMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll', 'updatePageNumber', 'updatePageSize', 'updateFilter' ],
      invoke: [
        {
          id: 'getCars',
          src: 'getCars',
          onDone: {
            actions: [ 'saveCars' ],
            target: 'finished',
          },
          onError: {
            actions: [ 'saveError' ],
            target: 'failed',
          },
        },
      ],
      exit: [ 'enableAll', 'updatePaginationOnPaginator' ],
    },
    finished: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
      },
    },
    failed: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    updatePageNumber: assign((context, { data: { pageNumber } = {} }) => {
      context.pagination.pageNumber = pageNumber;
    }),
    updatePageSize: assign((context, { data: { pageSize } = {} }) => {
      context.pagination.pageSize = pageSize || context.pagination.pageSize;
    }),
    updateFilter: assign((context, { data: { filter } = {} }) => {
      if (filter !== undefined) context.filter = { status: filter };
    }),
    updatePaginationOnPaginator: send(({ pagination }) => ({ type: 'UPDATE_PAGINATION', data: { pagination } }), { to: 'paginator' }),
    saveError: assign((context, event) => { context.error = event.data; }),
    saveCars: assign((context, event) => {
      context.cars = event.data.entities;
      context.pagination = event.data.pagination;
    }),
  },
  services: {
    getCars: async ({ pagination: { total, ...others }, filter }) => {
      const response = await requester.get(`/cars?${new URLSearchParams({ ...others, ...filter }).toString()}`);
      return response.data;
    },
  },
};

export default createMachine(machineDefinition, machineOptions);