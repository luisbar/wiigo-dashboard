import { actions, createMachine, send } from 'xstate';
import quotationForm from '@root/stateMachines/organisms/quotationForm';
import { assign } from '@xstate/immer';
import requester from '@utils/requester';

const CHILDREN_TO_DISABLED = [ 'quotationForm' ];

export const machineDefinition = {
  id: 'trip',
  initial: 'idle',
  context: {
    trip: undefined,
    error: {},
  },
  invoke: [
    {
      id: 'quotationForm',
      src: quotationForm,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            target: 'processing',
          },
        ],
        UPDATE_PRICE: [
          {
            actions: [ 'updatePrice' ],
            target: 'idle',
          },
        ],
      },
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: [
        {
          id: 'getTrip',
          src: 'getTrip',
          onDone: {
            target: 'finished',
            actions: [ 'saveTrip' ],
          },
          onError: {
            target: 'failed',
            actions: [ 'saveError' ],
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    failed: {
      after: {
        1: [
          {
            target: 'idle',
          },
        ],
      },
    },
  },
};

const machineOptions = {
  actions: {
    updatePrice: assign((context, event) => {
      context.trip.price = event.data.price;
      context.trip.status = event.data.status;
    }),
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    saveTrip: assign((context, event) => { context.trip = event.data; }),
    saveError: assign((context, event) => { context.error = event.data; }),
  },
  services: {
    getTrip: async (context, { data: { tripId } }) => {
      const response = await requester.get(`/trips/${tripId}`);
      return response.data;
    },
  },
};

export default createMachine(machineDefinition, machineOptions);