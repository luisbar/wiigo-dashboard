import { createMachine, actions, send, sendParent } from 'xstate';
import buttonMachine from '@stateMachines/atoms/button';
import inputMachine from '@stateMachines/atoms/input';
import schemaValidator from '@utils/schemaValidator';
import requester from '@utils/requester';
import { assign } from '@xstate/immer';

const INPUTS_CHILDREN = [ 'priceInput' ];
const CHILDREN_TO_DISABLED = [ ...INPUTS_CHILDREN, 'quotationButton' ];

export const machineDefinition = {
  id: 'quotationForm',
  initial: 'idle',
  context: {
    error: undefined,
  },
  invoke: [
    {
      id: 'priceInput',
      src: inputMachine,
    },
    {
      id: 'quotationButton',
      src: buttonMachine,
    },
  ],
  states: {
    idle: {
      on: {
        PROCESS: [
          {
            cond: 'dataIsValid',
            actions: [ 'cleanErrorOnInputs' ],
            target: 'processing',
          },
          {
            actions: [ 'cleanErrorOnInputs', 'showErrorOnInputs' ],
            target: 'idle',
          },
        ],
        DISABLE: {
          target: 'disabled',
        },
      },
    },
    disabled: {
      entry: [ 'disableAll' ],
      on: {
        ENABLE: {
          target: 'idle',
        },
      },
      exit: [ 'enableAll' ],
    },
    processing: {
      entry: [ 'disableAll' ],
      invoke: [
        {
          id: 'updateTrip',
          src: 'updateTrip',
          onDone: {
            target: 'finished',
            actions: [ 'sendDataToParent' ],
          },
          onError: {
            target: 'failed',
            actions: [ 'saveError' ],
          },
        },
      ],
      exit: [ 'enableAll' ],
    },
    finished: {
      after: {
        1: {
          target: 'idle',
        },
      },
    },
    failed: {
      after: {
        1: [
          {
            target: 'idle',
          },
        ],
      },
    },
  },
};

const machineOptions = {
  actions: {
    disableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'DISABLE' }, { to: child, delay: 1 }))),
    enableAll: actions.pure(() => CHILDREN_TO_DISABLED.map((child) => send({ type: 'ENABLE' }, { to: child }))),
    saveError: assign((context, event) => { context.error = event.data; }),
    cleanErrorOnInputs: actions.pure(() => INPUTS_CHILDREN.map((input) => send({ type: 'CLEAN_ERROR' }, { to: input }))),
    sendDataToParent: sendParent((context, event) => ({
      type: 'UPDATE_PRICE',
      data: {
        ...event.data,
      },
    })),
    showErrorOnInputs: actions
      .pure((context, event) => schemaValidator
        .validateQuotationData(event.data)
        .map((errorMessage) => {
          const targetInput = INPUTS_CHILDREN.filter((input) => errorMessage.includes(input.replace(/.(Input)$/, '')))[0];
          return send(
            { type: 'SHOW_ERROR', data: { error: errorMessage } },
            { to: targetInput, delay: 1 },
          );
        })),
  },
  guards: {
    dataIsValid: (context, event) => !schemaValidator.validateQuotationData(event.data),
  },
  services: {
    updateTrip: async (context, { data: { tripId, ...body } }) => {
      const response = await requester.patch(`/trips/${tripId}`, body);
      return response.data;
    },
  },
};

export default createMachine(machineDefinition, machineOptions);