import propTypes from 'prop-types';
import React, { createContext, useContext } from 'react';
import { useInterpret } from '@xstate/react';
import loginMachine from '@stateMachines/pages/login';
import tripsMachine from '@stateMachines/pages/trips';
import tripMachine from '@stateMachines/pages/trip';
import carsMachine from '@stateMachines/pages/cars';

const StoreContext = createContext([]);
const StoreProvider = ({ children }) => {
  const loginService = useInterpret(loginMachine, { devTools: process.env.ENV === 'development' });
  const tripsService = useInterpret(tripsMachine, { devTools: process.env.ENV === 'development' });
  const tripService = useInterpret(tripMachine, { devTools: process.env.ENV === 'development' });
  const carsService = useInterpret(carsMachine, { devTools: process.env.ENV === 'development' });

  return (
    <StoreContext.Provider
      value={{ loginService, tripsService, tripService, carsService }}
    >
      {children}
    </StoreContext.Provider>
  );
};

StoreProvider.propTypes = {
  children: propTypes.node.isRequired,
};

export default StoreProvider;
export const useStoreContext = () => useContext(
  StoreContext,
);