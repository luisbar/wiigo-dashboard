import { useEffect } from 'react';

const useDisabled = ({ disabled, state, send }) => {
  useEffect(() => {
    if (!disabled && state.matches('disabled')) send({
      type: 'ENABLE',
    });

    if (disabled && state.matches('enabled')) send({
      type: 'DISABLE',
    });
  }, [ disabled ]);
};

export default useDisabled;