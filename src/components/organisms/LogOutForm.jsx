import React, { useEffect } from 'react';
import Image from '@atoms/Image';
import Text from '@atoms/Text';
import Button from '@atoms/Button';
import { useActor } from '@xstate/react';
import { useNavigation } from 'react-navi';
import { useStoreContext } from '@stateMachines/Store';

const LogOutForm = () => {
  const { tripsService, loginService } = useStoreContext();
  const [ state ] = useActor(tripsService.children.get('logOutForm'));
  const navigation = useNavigation();

  useEffect(() => {
    if (state.matches('finished')) navigation.navigate('/login');
  }, [ state.value ]);

  return (
    <div
      className='flex flex-col items-center pt-5'
    >
      <Image
        src='https://avatars.dicebear.com/api/jdenticon/luisangelb.svg'
        className='w-12 rounded-full'
      />
      <Text
        label={loginService.state.context.user.firstName}
        className='pb-2.5 pt-2.5 text-white'
      />

      <Button
        className='bg-light-100'
        disabled={state.matches('logOut')}
        text={{
          className: 'text-accent-100',
          label: 'logOutForm.txt1',
        }}
        data={{
          type: 'PROCESS',
        }}
        machine={state.children.logOutButton}
      />
    </div>
  );
};

export default LogOutForm;