import React from 'react';
import propTypes from 'prop-types';
import TableCell from '@molecules/TableCell';

const Table = ({ columns, rows, rowsCount, className }) => {
  const renderColums = () => columns.map(({ value }, columnIndex) => (
    <TableCell
      key={`column-${value}`}
      type='header'
      label={value}
      columnIndex={columnIndex}
    />
  ));

  const renderRows = () => (
    <div
      className={`grid grid-rows-table-body-${rowsCount} grid-cols-${columns.length} col-start-1 col-end-${columns.length + 1} overflow-y-scroll`}
    >
      {
          rows.map((row, rowIndex) => columns.map(({ key, type, actions }, columnIndex) => (
            <TableCell
              key={`cell-${row.id}-${key}`}
              type={type}
              row={row}
              rowIndex={rowIndex}
              columnKey={key}
              actions={actions}
              columnIndex={columnIndex}
            />
          )))
        }
    </div>
  );

  return (
    <div className={`shadow-md grid grid-rows-table grid-cols-${columns.length} col-start-1 col-end-${columns.length + 1} ${className}`}>
      {renderColums()}
      {renderRows()}
    </div>
  );
};

Table.propTypes = {
  columns: propTypes.array.isRequired,
  rows: propTypes.array.isRequired,
  rowsCount: propTypes.number.isRequired,
  className: propTypes.string,
};

Table.defaultProps = {
  className: '',
};

export default Table;