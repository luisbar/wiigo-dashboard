import React, { useState } from 'react';
import Dropdown from '@atoms/Dropdown';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';

const FilterForm = ({ machine, defaultPageSize, defaultFilter }) => {
  const [ state ] = useActor(machine);
  const [ pageSize, setPageSize ] = useState(defaultPageSize);
  const [ filter, setFilter ] = useState(Object.values(defaultFilter).length ? Object.values(defaultFilter)[0] : '');

  const onChangePageSize = ({ target }) => {
    setPageSize(target.value);
  };

  const onChangeFilter = ({ target }) => {
    setFilter(target.value);
  };

  return (
    <>
      <Dropdown
        machine={state.children.pageSizeDropdown}
        className='row-start-1 col-start-1 self-end'
        text={{
          label: 'filterForm.txt1',
        }}
        select={{
          onChange: onChangePageSize,
          options: [
            {
              key: 'filterForm.txt4',
              value: 'filterForm.txt4',
            },
            {
              key: 'filterForm.txt5',
              value: 'filterForm.txt5',
            },
            {
              key: 'filterForm.txt6',
              value: 'filterForm.txt6',
            },
          ],
          defaultValue: pageSize,
        }}
      />
      <Input
        machine={state.children.filterInput}
        className='row-start-1 col-start-2 self-end'
        input={{
          type: 'text',
          onChange: onChangeFilter,
          defaultValue: filter,
          id: 'filter',
        }}
        text={{
          label: 'filterForm.txt2',
        }}
      />
      <Button
        machine={state.children.filterButton}
        className='row-start-1 col-start-3 self-end'
        text={{
          label: 'filterForm.txt3',
        }}
        data={{
          type: 'PROCESS',
          data: {
            pageSize,
            filter,
            pageNumber: 0,
          },
        }}
      />
    </>
  );
};

FilterForm.propTypes = {
  machine: propTypes.object.isRequired,
  defaultPageSize: propTypes.number.isRequired,
  defaultFilter: propTypes.object.isRequired,
};

export default FilterForm;