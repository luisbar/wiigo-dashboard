import propTypes from 'prop-types';
import React, { useState } from 'react';
import Input from '@atoms/Input';
import Button from '@atoms/Button';
import { useStoreContext } from '@root/stateMachines/Store';
import { useActor } from '@xstate/react';

const QuotationForm = ({ className, tripId }) => {
  const { tripService } = useStoreContext();
  const [ state ] = useActor(tripService.children.get('quotationForm'));
  const [ price, setPrice ] = useState(undefined);

  const onChange = ({ target }) => {
    setPrice(() => target.value);
  };

  return (
    <div
      className={`flex flex-col ${className}`}
    >
      <Input
        text={{
          label: 'quotationForm.txt1',
        }}
        input={{
          type: 'text',
          onChange,
          id: 'price',
        }}
        machine={state.children.priceInput}
      />
      <Button
        className='mt-5'
        machine={state.children.quotationButton}
        text={{
          label: 'quotationForm.txt2',
        }}
        data={{
          type: 'PROCESS',
          data: {
            price,
            tripId,
            status: 'QUOTED',
          },
        }}
      />
    </div>
  );
};

QuotationForm.propTypes = {
  className: propTypes.string.isRequired,
  tripId: propTypes.string.isRequired,
};

export default QuotationForm;