import React, { createContext, useContext } from 'react';
import { useMachine } from '@xstate/react';
import machine from '@stateMachines/atoms/globalSpinner';
import propTypes from 'prop-types';

const GlobalSpinner = ({ className }) => (
  <div className={`bg-primary h-1 w-2/4 move-horizontal fixed ${className}`} />
);

GlobalSpinner.propTypes = {
  className: propTypes.string,
};

GlobalSpinner.defaultProps = {
  className: '',
};

const GlobalSpinnerContext = createContext([]);
const GlobalSpinnerProvider = ({ children }) => {
  const [ state, send ] = useMachine(machine, { devTools: process.env.ENV === 'development' });

  return (
    <GlobalSpinnerContext.Provider value={[ state, send ]}>
      {state.matches('enabled') && <GlobalSpinner className={state?.event?.data?.className} />}
      {children}
    </GlobalSpinnerContext.Provider>
  );
};

GlobalSpinnerProvider.propTypes = {
  children: propTypes.node.isRequired,
};

export default GlobalSpinnerProvider;
export const useGlobalSpinnerContext = () => useContext(GlobalSpinnerContext);
