import propTypes from 'prop-types';
import React from 'react';
import SideMenu from '@organisms/SideMenu';

const Main = ({ children }) => (
  <div
    className='w-full h-full flex flex-row'
  >
    <SideMenu
      items={[
        {
          path: '/trips',
          value: 'sideMenu.txt1',
        },
        {
          path: '/cars',
          value: 'sideMenu.txt2',
        },
      ]}
    />
    <div
      className='w-full overflow-y-scroll grid grid-rows-main grid-cols-4 gap-y-5 gap-x-5 p-5'
    >
      {children}
    </div>
  </div>
);

Main.propTypes = {
  children: propTypes.node.isRequired,
};

export default Main;