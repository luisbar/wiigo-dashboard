import propTypes from 'prop-types';
import React from 'react';

const Centered = ({ children }) => (
  <div className='mx-auto flex flex-col justify-around items-center bg-primary'>
    {children}
  </div>
);

Centered.propTypes = {
  children: propTypes.node.isRequired,
};

export default Centered;