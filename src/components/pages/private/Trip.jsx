import React, { useEffect } from 'react';
import Main from '@templates/Main';
import Card from '@molecules/Card';
import Map from '@molecules/Map';
import { flatten } from 'ramda';
import QuotationForm from '@root/components/organisms/QuotationForm';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import { useStoreContext } from '@root/stateMachines/Store';
import { useActor } from '@xstate/react';
import propTypes from 'prop-types';

const Trip = ({ route: { lastChunk: { request: { body } } } }) => {
  const { tripService } = useStoreContext();
  const [ state, send ] = useActor(tripService);
  const [ quotationFormState ] = useActor(tripService.children.get('quotationForm'));
  const trip = { ...body, ...state.context.trip };
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();

  useEffect(() => {
    send({
      type: 'PROCESS',
      data: {
        tripId: body.id,
      },
    });
  }, []);

  useEffect(() => {
    if (state.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!state.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (state.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: state.context.error.message } });
  }, [ state.value ]);

  useEffect(() => {
    if (quotationFormState.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!quotationFormState.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (quotationFormState.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: quotationFormState.context.error.message } });
    if (quotationFormState.matches('finished')) toggleAlert({ type: 'ENABLE', data: { type: 'success', message: 'trip.txt8' } });
  }, [ quotationFormState.value ]);

  return (
    <Main>
      <Card
        className='col-start-1'
        title='trip.txt1'
        items={[
          {
            key: 'trip.txt2',
            value: `${trip.user.firstName} ${trip.user.lastName}`,
          },
          {
            key: 'trip.txt3',
            value: trip.user.email,
          },
        ]}
      />
      <Card
        className='col-start-2'
        title='trip.txt4'
        items={[
          {
            key: 'trip.txt5',
            value: `${trip.amountOfPersons}`,
          },
          {
            key: 'trip.txt6',
            value: trip.status,
          },
          {
            key: 'trip.txt7',
            value: trip.price,
          },
        ]}
      />
      <Map
        locations={flatten(trip.rides.map(({ origin, destiny }) => [ origin, destiny ]))}
      />
      {
        trip.status === 'QUOTE_PENDING'
        && (
        <QuotationForm
          className='row-start-2 col-start-1 col-span-2'
          tripId={trip.id}
        />
        )
      }
    </Main>
  );
};

Trip.propTypes = {
  route: propTypes.object.isRequired,
};

export default Trip;