import React, { useEffect } from 'react';
import Main from '@templates/Main';
import Table from '@organisms/Table';
import { useGlobalSpinnerContext } from '@atoms/GlobalSpinner';
import { useGlobalAlertContext } from '@molecules/GlobalAlert';
import Paginator from '@molecules/Paginator';
import FilterForm from '@organisms/FilterForm';
import { useStoreContext } from '@stateMachines/Store';
import { useNavigation } from 'react-navi';
import { useActor } from '@xstate/react';

const Trips = () => {
  const { tripsService } = useStoreContext();
  const [ state, send ] = useActor(tripsService);
  const toggleSpinner = [ ...useGlobalSpinnerContext() ].pop();
  const toggleAlert = [ ...useGlobalAlertContext() ].pop();
  const navigator = useNavigation();
  const onClickItem = (data) => {
    navigator.navigate(`/trips/${data.id}`, { body: data });
  };

  useEffect(() => {
    if (state.matches('processing')) toggleSpinner({ type: 'ENABLE' });
    if (!state.matches('processing')) toggleSpinner({ type: 'DISABLE' });
    if (state.matches('failed')) toggleAlert({ type: 'ENABLE', data: { type: 'error', message: state.context.error.message } });
  }, [ state.value ]);

  useEffect(() => {
    send({
      type: 'PROCESS',
      data: {
        pageNumber: state.context.pagination.pageNumber,
        pageSize: state.context.pagination.pageSize,
      },
    });
  }, []);

  return (
    <Main>
      <FilterForm
        machine={state.children.filterForm}
        defaultPageSize={parseInt(state.context.pagination.pageSize, 10)}
        defaultFilter={state.context.filter}
      />
      <Table
        className='row-start-2'
        columns={[
          { key: 'id', value: 'trips.txt1', type: 'text' },
          { key: 'created', value: 'trips.txt2', type: 'text' },
          { key: 'user.firstName', value: 'trips.txt3', type: 'text' },
          { key: 'status', value: 'trips.txt4', type: 'text' },
          { key: 'actions', value: 'trips.txt5', type: 'actions', actions: [ { icon: 'MdRemoveRedEye', onClickItem } ] },
        ]}
        rows={state.context.trips}
        rowsCount={parseInt(state.context.pagination.pageSize, 10)}
      />
      <Paginator
        machine={state.children.paginator}
        className='row-start-3 col-start-4 col-end-6'
      />
    </Main>
  );
};

export default Trips;
