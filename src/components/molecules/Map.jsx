import propTypes from 'prop-types';
import React from 'react';
import { MapContainer, TileLayer, Marker, Popup, Polyline } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import marker from '@images/marker.svg';

const Map = ({ locations }) => {
  const renderMarkers = () => locations
    .map(({ latitude, longitude, name, formattedAddress }) => (
      <Marker
        key={`marker-${latitude}-${longitude}`}
        position={[ latitude, longitude ]}
        icon={new L.Icon({
          iconUrl: marker,
          iconRetinaUrl: marker,
          iconSize: new L.Point(30, 30),
        })}
      >
        <Popup>
          {`${name}, ${formattedAddress}`}
        </Popup>
      </Marker>
    ));

  return (
    <MapContainer
      className='row-start-1 row-end-4 col-start-3 col-end-5 w-full rounded-md z-0'
      center={[ locations[0].latitude, locations[0].longitude ]}
      zoom={5}
      zoomControl={false}
      minZoom={5}
    >
      <TileLayer
        url='https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png'
      />
      {renderMarkers()}
      <Polyline
        positions={locations.map(({ latitude, longitude }) => [ latitude, longitude ])}
        pathOptions={{ color: '#FFFFFF' }}
      />
    </MapContainer>
  );
};

Map.propTypes = {
  locations: propTypes.arrayOf(
    propTypes.shape({
      latitude: propTypes.string.isRequired,
      longitude: propTypes.string.isRequired,
      name: propTypes.string.isRequired,
      formattedAddress: propTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
};

export default Map;