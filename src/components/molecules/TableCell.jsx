import React from 'react';
import * as icons from 'react-icons/md';
import Text from '@atoms/Text';
import propTypes from 'prop-types';

const getValueUsingDotNotation = ({ columnKey, row }) => columnKey.split('.').reduce((accumulator, item) => accumulator[item], row);

const onClick = ({ row, onClickItem }) => () => {
  onClickItem(row);
};

const Cell = {
  header: ({ columnIndex, label }) => (
    <Text
      className={`row-start-1 col-start-${columnIndex + 1} self-center justify-self-center`}
      label={label}
      as='h6'
    />
  ),
  text: ({ rowIndex, columnIndex, row, columnKey }) => (
    <Text
      className={`row-start-${rowIndex + 1} col-start-${columnIndex + 1} self-center justify-self-center`}
      label={getValueUsingDotNotation({ columnKey, row })}
    />
  ),
  actions: ({ rowIndex, columnIndex, row, actions }) => (
    <div
      className={`row-start-${rowIndex + 1} col-start-${columnIndex + 1} self-center justify-self-center`}
    >
      {
        actions.map(({ icon, onClickItem }) => {
          const Icon = icons[icon];
          return (
            <Icon
              className='cursor-pointer text-accent-100'
              key={`action-${rowIndex}`}
              onClick={onClick({ row, onClickItem })}
            />
          );
        })
      }
    </div>
  ),
};

Cell.header.propTypes = {
  columnIndex: propTypes.number.isRequired,
  label: propTypes.string.isRequired,
};

Cell.text.propTypes = {
  rowIndex: propTypes.number.isRequired,
  columnIndex: propTypes.number.isRequired,
  row: propTypes.string.isRequired,
  columnKey: propTypes.string.isRequired,
};

Cell.actions.propTypes = {
  rowIndex: propTypes.number.isRequired,
  columnIndex: propTypes.number.isRequired,
  row: propTypes.string.isRequired,
  actions: propTypes.arrayOf(propTypes.shape({
    icon: propTypes.string,
    onClickItem: propTypes.func,
  })).isRequired,
};

const TableCell = ({ type, ...others }) => {
  const renderCell = Cell[type];
  return renderCell({ ...others });
};

TableCell.propTypes = {
  type: propTypes.string.isRequired,
};

export default TableCell;