import propTypes from 'prop-types';
import React from 'react';
import Text from '@atoms/Text';

const Card = ({ title, items, className }) => (
  <div
    className={`p-5 rounded-md shadow-md ${className}`}
  >
    <Text
      as='p'
      className='bg-accent-200 rounded-md p-1 mb-4 inline-block'
      label={title}
    />
    {
        items.map(({ key, value }) => (
          <div
            key={`card-item-${key}`}
            className='flex flex-row'
          >
            <Text
              className='mr-2'
              label={key}
            />
            <Text
              className='truncate'
              label={value || '-'}
            />
          </div>
        ))
      }
  </div>
);

Card.propTypes = {
  className: propTypes.string,
  items: propTypes.arrayOf(
    propTypes.shape({
      key: propTypes.string.isRequired,
      value: propTypes.string.isRequired,
    }).isRequired,
  ).isRequired,
  title: propTypes.string.isRequired,
};

Card.defaultProps = {
  className: '',
};

export default Card;